class SectionCreater {
    create(type) {
        if (type === 'standard') {
            let headerContent = document.createTextNode('Join Our Program');
            let formButtonText = document.createTextNode('Subscribe');
    
            return new Section(headerContent, formButtonText).constructSection();
        }
        
        if (type === 'advanced') {
            let headerContent = document.createTextNode('Join Our Advanced Program');
            let formButtonText = document.createTextNode('Subscribe to Advanced Program');
    
            return new Section(headerContent, formButtonText).constructSection();
        }
    }
}

class Section {
    constructor(headerContent, formButtonText) {
            this.headerContent = headerContent;
            this.formButtonText = formButtonText;
            this.joinSection = document.createElement('section');
            this.sectionHeader = document.createElement('h2');
            this.sectionSubtitle = document.createElement('h3');
            this.subtitleContent1 = document.createTextNode('Sed do eiusmod tempor incididunt');
            this.subtitleContent2 = document.createTextNode('ut labore et dolore magna aliqua.');
            this.formContainer = document.createElement('form');
            this.emailForm = document.createElement('input');
            this.formButton = document.createElement('button');
    }

    constructSection() {
        let learnMore = document.querySelector('section.app-section.app-section--image-culture');


        let joinSection = document.createElement('section');
        joinSection.className = 'app-section';
        learnMore.parentNode.insertBefore(joinSection, learnMore.nextSibling);
    
        let sectionHeader = document.createElement('h2');
        sectionHeader.className = 'app-title';
    
        sectionHeader.appendChild(this.headerContent);
        joinSection.appendChild(sectionHeader);
    
        let sectionSubtitle = document.createElement('h3');
        sectionSubtitle.className = 'app-subtitle';
        let subtitleContent1 = document.createTextNode('Sed do eiusmod tempor incididunt');
        let subtitleContent2 = document.createTextNode('ut labore et dolore magna aliqua.');
    
        sectionSubtitle.appendChild(subtitleContent1);
        sectionSubtitle.appendChild(document.createElement('br'));
        sectionSubtitle.appendChild(subtitleContent2);
        joinSection.appendChild(sectionSubtitle);
    
        let formContainer = document.createElement('form');
        formContainer.className = 'app-form';
        let emailForm = document.createElement('input');
        emailForm.className = 'app-form__email-input';
        emailForm.setAttribute('type', 'email');
        emailForm.setAttribute('name', 'email');
        emailForm.setAttribute('placeholder', 'Email');
    
        joinSection.appendChild(formContainer);
        formContainer.appendChild(emailForm);
    
        let formButton = document.createElement('button');
        formButton.className = 'app-section__button app-section__button--subscribe';
        formButton.appendChild(this.formButtonText);
    
        formContainer.appendChild(formButton);
    
        function media(width) {
            if (width.matches) {
                joinSection.style.height = 525 + 'px';
    
                emailForm.style.width = 80 + 'vw';
    
                formContainer.style.display = 'flex';
                formContainer.style.justifyContent = 'center';
                formContainer.style.alignItems = 'center';
                formContainer.style.flexDirection = 'column';
    
                formButton.style.margin = 47 + 'px auto';
    
            } else {
                joinSection.style.height = 436 + 'px';
                joinSection.style.background = 'url(../assets/images/join-bg-photo.png) no-repeat center/cover';
                joinSection.style.color = 'white';
    
                formContainer.style.marginTop = 41 + 'px';
                formContainer.style.flexDirection = 'row';
    
                emailForm.style.all = 'unset';
                emailForm.style.width = 'calc(1200px/3)';
                emailForm.style.padding = .5 + 'em ' + 1 + 'em';
                emailForm.style.backgroundColor = 'rgba(255, 255, 255, .15)';
                emailForm.style.fontWeight = 400;
    
                formButton.style.borderRadius = 26 + 'px';
                formButton.style.fontSize = 14 + 'px';
                formButton.style.height = 36 + 'px';
                formButton.style.width = 135 + 'px';
                formButton.style.letterSpacing = 1.2 + 'px';
                formButton.style.marginLeft = 30 + 'px';
                formButton.style.textTransform = 'uppercase';
            }
        }
    
        let width = window.matchMedia("(max-width: 768px)");
        media(width); // Call listener function at run time
        width.addEventListener('change', media); // Attach listener function on state changes
    
        formContainer.addEventListener('submit', function (e) {
            e.preventDefault();
            console.log(emailForm.value);
        }, false);
    }
}

export { SectionCreater };