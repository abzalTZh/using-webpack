import { SectionCreater} from './join-us-section.js';
import { validate } from './email-validator';
import '../styles/style.css';
import '../styles/normalize.css';

let newSection = new SectionCreater;

window.addEventListener('load', newSection.create('advanced'), false);

let form = document.querySelector('form.app-form');
let email = document.querySelector('input.app-form__email-input')

form.addEventListener('submit', () => {
    alert(validate(email.value));
})