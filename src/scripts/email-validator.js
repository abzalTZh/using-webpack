const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

export function validate(email) {
    if (email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1])) {
        return true;
    } else return false;
}